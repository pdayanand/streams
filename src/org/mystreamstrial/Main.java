package org.mystreamstrial;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void countTwos() {
        List<Integer> input = Arrays.asList(1,2,2,4,2,5);

        System.out.println(input.stream().filter(i -> i==2).count());
    }

    public static void filterOutString() {
        List<String> input = new ArrayList<>(Arrays.asList("abc"," " , "bcd"," " , "defg" ,"jk"));

        System.out.println(input.stream().filter(str -> str.length() > 2).collect(Collectors.toList()).toString());
    }

    public static void upperCaseAppend() {
        List<String> input = new ArrayList<>(Arrays.asList( "USA", "Japan", "France", "Germany", "India", "U.K.","Canada"));

        System.out.println(input.stream().map(String::toUpperCase).collect(Collectors.joining(",")));

    }

    public static void squareDistinct() {
        List<Integer> input = Arrays.asList( 9, 10, 3, 4, 7, 3, 4);

        System.out.println(input.stream().distinct().map(i -> i*i).collect(Collectors.toList()));
    }

    public static void main(String[] args) {

        countTwos();
        filterOutString();
        upperCaseAppend();
        squareDistinct();
    }
}
